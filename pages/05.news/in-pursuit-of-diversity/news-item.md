---
title: 'In pursuit of diversity'
date: '20-02-2018 00:00'

taxonomy:
    category: news
    tag: [sample, demo, diversity]

hero_classes: 'hero-large text-light'
hero_image: Corporate-Diversity.jpg
news_url: /news

show_sidebar: true
show_breadcrumbs: true
show_pagination: true
blog_url: /blog
---

In its pursuit for growth and diversity, Neovince has expanded its services beyond advertising and signage. The company recently ventured in construction design & materials for architectural and general lighting, metal works and fit outs, acrylic items and even furniture. Its growing clientele includes banks, restaurants, commercial and residential properties, hotels, and casinos. Neovince's comprehensive network of suppliers and contractors, allows the company to tailor-fit services to suit a diverse set of customer needs.

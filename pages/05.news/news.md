---
title: News
sitemap:
    changefreq: monthly
body_classes: header-fixed
hero_classes: 'text-light title-h1h2 hero-medium '
news_url: /news
show_sidebar: false
show_breadcrumbs: true
show_pagination: true
content:
    items:
        - '@self.children'
    limit: 5
    order:
        by: date
        dir: desc
    pagination: true
    url_taxonomy_filters: true
feed:
    description: 'Sample news Description'
    limit: 10
pagination: true
---

# News

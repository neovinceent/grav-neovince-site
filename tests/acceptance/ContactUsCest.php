<?php

class ContactUsCest
{
    public function _before(\AcceptanceTester $I)
    {
    }

    public function _after(\AcceptanceTester $I)
    {
    }

    public function contactUsForm(\AcceptanceTester $I)
    {
        $I->wantTo('send a message via Contact Us Form');
        $I->amOnPage('/contact-us');
        $I->fillField("input#name", 'Tester');
        $I->fillField('input#email', 'test@example.com');
        $I->fillField('textarea#message', 'Request Feedback');
        $I->click('Submit');
        $I->see('Thank you!');
    }
}

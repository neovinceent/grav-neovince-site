---
title: 'Sky Signage Maker'
subtitle: 'light the sky'
date: '31-08-2013 00:00'

taxonomy:
    category: news
    tag: [sample, demo, signage]

hero_classes: 'hero-large text-light'
hero_image: IMG_0760-1024x768.jpg
---

Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.

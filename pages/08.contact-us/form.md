---
title: 'Contact Us'
content:
    items: '@self.modular'
    order:
        by: default
        dir: asc
onpage_menu: false
form:
    name: contact-us
    action: /contact-us
    fields:
        - name: name
          id: name
          label: Name
          classes: null
          placeholder: 'Enter your name'
          autocomplete: 'on'
          type: text
          validate:
              required: true
        - name: email
          id: email
          classes: null
          label: Email
          placeholder: 'Enter your email address'
          type: email
          validate:
              rule: email
              required: true
        - name: message
          id: message
          label: Message
          classes: null
          size: long
          placeholder: 'Enter your message'
          type: textarea
          validate:
              required: true
    buttons:
        - type: submit
          value: Submit
          classes: 'btn btn-primary'
        - type: reset
          value: Reset
          classes: 'btn'
    process:
        - email:
            from: '{{ config.plugins.email.from }}'
            to: '{{ form.value.email }}'
            cc: '{{ config.plugins.email.from }}'
            subject: '[Feedback] {{ form.value.name|e }}'
            body: "{% include 'forms/data.html.twig' %}"
        - save:
            fileprefix: feedback-
            dateformat: Ymd-His-u
            extension: txt
            body: "{% include 'forms/data.txt.twig' %}"
        - message: 'Thank you for your feedback!'
        - display: thankyou
---

## We want to hear from you!

Have a comment or question?  Please send us a message.
